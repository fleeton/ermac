#Install guide

#Supposed to run
ec2-run-instances ami-05355a6c -t m1.large -g [SECURITY-GROUP] -k [KEY-PAIR] -b "/dev/xvdf=:200:false:io1:1000" -b "/dev/xvdg=:25:false:io1:250" -b "/dev/xvdh=:10:false:io1:100" --ebs-optimized true

#Ran this instead.
ec2-run-instances ami-07c91c64 -t m1.large -g sg-a9d905cd -k ec-private/aws-mongo-wiredTiger.pem -b "/dev/xvdf=:200:false:io1:1000" -b "/dev/xvdg=:25:false:io1:250" -b "/dev/xvdh=:10:false:io1:100" --subnet subnet-ec8d8f9b --ebs-optimized true


security group :  sg-a9d905cd

#List of things I did to setup the Mongo Database in AWS

1. Made an instance m1.large on Viginia region with the available AMI : ami-05355a6c.
2. Made an AMI (snapshot) image of this instance.
3. Copied this AMI to the Singapore region.
4. Terminated the Virginia Instance.
5. Deregistered the Virginia AMI.
6. Deleted the Virginia Snapshot.
7. Followed the install walkthrough guide of mongodb from : https://docs.mongodb.org/ecosystem/platforms/amazon-ec2/

-- Mongo Database 2.6.12 has been installed now --

8. Updates this version to 3.0 manually by replacing the binaries at /usr/bin of mongodb.

-- Mongo Database 3.0 has been installed now --

9. Updates the version 3.0 to 3.2 manually by replacing the binaries at /usr/bin.

-- Mongo Database 3.2 has been installed neow --

10. Starts the mongo db.

-- Up and Running.


PS :: Created a private subnet first [along with it's routing table]