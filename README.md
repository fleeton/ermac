# Mongodb configuration file
Uses wiredTiger as storage engine.

---
The database can be started by running - 
```sh
$ cd /usr/bin
$ ./mongod -f path-to-config-yaml
```
---
To shutdown the database -
```
$ cd /usr/bin
$ ./mongod --dbpath /data/mongodb --shutdown
```
---